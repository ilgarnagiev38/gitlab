import '~/pages/projects';
import { shouldHandRaiseLeadMount, initHandRaiseLead } from 'ee/hand_raise_leads/hand_raise_lead';

shouldHandRaiseLeadMount();
initHandRaiseLead();
